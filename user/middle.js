// const csv = require("csv-parser");
// const { json } = require("body-parser");
const fs = require("fs");

const connection = require("../DataBase/db");


module.exports.middle = (req, res, next) => {
  const { firstName, lastName, email, password } = req.body;
  let errors = [];
  if (!firstName || firstName == null || firstName.trim().length === 0)
    errors.push("firstName  required");
  if (!lastName || lastName == null || lastName.trim().length === 0)
    errors.push("lastName  required");
  if (!email || email == null || email.trim().length === 0)
    errors.push("email is required");
  if (!password || password == null || password.trim().length === 0)
    errors.push("password Number required");

  if (errors.length > 0) {
    return res.status(400).send({ msg: "Some Data Missing", errors });
  }

  req.body.firstName = firstName.trim();
  req.body.lastName = lastName.trim();
  req.body.email = email.trim();
  req.body.password = password.trim();


  // write the data on CSV file
const createCsvWriter = require('csv-writer').createObjectCsvWriter
const csvWriter = createCsvWriter({
  path: 'out.csv',
  header: [
    {id: 'Method', title: 'Methods'},
    {id: 'URL', title: 'URL'},
    {id: 'Params', title: 'Params'},
    {id: 'Date', title: 'Date'},
    // {id: 'TimeStamp', title: 'TimeStamp'}
  ],  append: true 
});

const data = [
 {Method: req.method,
  URL: req.url,
  Params: JSON.stringify(req.body),
  Date: new Date().toString()
}
];
console.log(req.body, typeof(req.body))

csvWriter.writeRecords(data)
.then(()=> console.log('The CSV file was written successfully'));

// const csv = json2csv.Parse(data);

// fs.appendFile('out.csv', JSON.stringify(data), (err) => {
//   if (err) console.error('Couldn\'t append the data');
//   console.log('The data was appended to file!');
// });



  next();
};
