const {addNewUser, addNewImage}  = require('./service')

module.exports.addUser = (req, res) => { 
    addNewUser(req.body, (error, response) => {
        if(error) return res.status(400).send({ msg: 'Something went wrong', err: error})
        res.status(200).send({
            msg:' Data inserted Successfully',
            data: response
        })
    })
}

module.exports.addImage = (req, res) => {
    addNewImage(req.body, (error, response) => {
        if(error) return res.status(400).send({msg: ' Something went wrong ', er: error})
        res.status(200).send({
            msg: ' Image inserted successfully ',
            data: response
        })
    })
}