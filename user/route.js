const router = require('express').Router();
const authorized = require('./middle')

const{addUser, addImage} = require('./controller');

router.post('/user', authorized.middle, addUser);



module.exports = router;