// Refrence => https://stackabuse.com/reading-and-writing-csv-files-with-node-js/

// Reading CSV Files in Node.js  

// to parse the content of the csv file we have used the npm i -s csv-parser for reading file
                            
                            // const csv = require('csv-parser');
                            // const fs = require('fs');


                            // fs.createReadStream('data.csv').pipe(csv()).on('data', (row) => {
                            //     console.log(row);
                            // }).on('end', () => {
                            //     console.log('csv file successfully processed ')
                            // })
                            
// we create a readStream using the fs module, pipe it into the csv object that will then fire the data event each time a new row from the CSV file is processed. The end event is triggered when all the rows from the CSV file are processed and we log a short message to the console to indicate that.

// Writing CSV Files in Node.js
/*
const createCsvWriter = require('csv-writer').createObjectCsvWriter; // importing the module
const csvWriter = createCsvWriter({ // creating the 
  path: 'out.csv',
  header: [
    {id: 'name', title: 'Name'},
    {id: 'surname', title: 'Surname'},
    {id: 'age', title: 'Age'},
    {id: 'gender', title: 'Gender'},
  ]
});

const data = [
  {
    name: 'John',
    surname: 'Snow',
    age: 26,
    gender: 'M'
  }, {
    name: 'Clair',
    surname: 'White',
    age: 33,
    gender: 'F',
  }, {
    name: 'Fancy',
    surname: 'Brown',
    age: 78,
    gender: 'F'
  }
];

csvWriter
  .writeRecords(data)
  .then(()=> console.log('The CSV file was written successfully'));
*/


//  The csv-writer module requires an initial configuration where we provide it with the name of the resulting CSV file and the header          configuration.
// Note: In our JavaScript object, all properties are in lowercase, but in the CSV file the first letters of them should be capitalized.

// After the config is done, all we need to do is call the writeRecords function, pass in the data array that represents the data structure that should be written to the CSV file.

// Once this process is done, we'll print an informational message to the console stating that the program has completed.

