
const csv = require("csv-parser");
const fs = require("fs");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const connection = require("../DataBase/db");
const async = require('async')

module.exports.middle = (req, res, next) => {
  const { firstName, lastName, email, password } = req.body;
  let errors = [];
  if (!firstName || firstName == null || firstName.trim().length === 0)
    errors.push("firstName  required");
  if (!lastName || lastName == null || lastName.trim().length === 0)
    errors.push("lastName  required");
  if (!email || email == null || email.trim().length === 0)
    errors.push("email is required");
  if (!password || password == null || password.trim().length === 0)
    errors.push("password Number required");

  if (errors.length > 0) {
    return res.status(400).send({ msg: "Some Data Missing", errors });
  }

  req.body.firstName = firstName.trim();
  req.body.lastName = lastName.trim();
  req.body.email = email.trim();
  req.body.password = password.trim();

  // write the data on CSV file
  const sqlData = "SELECT * FROM userbasicinfo";
  // console.log(sqlData);

  

  connection.query(sqlData, (err, data) => {
    if (err) {
      return err;
    } else {
      // console.log("sqlData***", data);
      // let reqData = {
      //   firstName: req.body.firstName,
      //   lastName: req.body.lastName,
      //   email: req.body.email,
      //   password: req.body.password,
      //   time: new Date(),
      // };

      // const userArr = data;
      // userArr.push(reqData);

      const csvWriter = createCsvWriter({
        path: "data.csv",
        header: [
          { id: "firstName", title: "First Name" },
          { id: "lastName", title: "Last Name" },
          { id: "email", title: "Eamil ID" },
          { id: "password", title: "Password" },
          { id: "time", title: "Time Stamp" },
        ],
      });

      let usersArr=[];
      for (let i = 0; i < data.length; i++) {
        // const userData = data[i];
      
        // async.eachSeries(data, function(item, clbk){
          // console.log(item)
          let obj = {
            firstName: data[i].firstName,
            lastName: data[i].lastName,
            email: data[i].email,
            password: data[i].password,
            time: new Date(),
          };
          usersArr.push(obj)
          
        // }, function(err){
        //   console.log("The CSV file was written successfully")
          // fs.createReadStream('data.csv').pipe(csv()).on('data', (row) => {
          //     console.log(row);
          // }).on('end', () => {
          //     console.log(' csv file successfully Read ')
          // })
          
        // })
        // const data1 = [
        //   {
        //     firstName: req.body.firstName,
        //     lastName: req.body.lastName,
        //     email: req.body.email,
        //     password: req.body.password,
        //     time: new Date()
        //   }
        // ];

        // csvWriter
        //   .writeRecords(data[i])
        //   .then(() => console.log("The CSV file was written successfully"));
      }
      let reqData = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        time: new Date(),
      };
      usersArr.push(reqData)

      csvWriter
          .writeRecords(usersArr)
          .then((csvData) => 
            console.log("csv data", csvData)
          ).catch((err)=>{ console.log("csv data", csvData) });

          next();
    }
  });

  // create the CSV file

  // fs.createReadStream('data.csv').pipe(csv()).on('data', (row) => {
  //     console.log(row);
  // }).on('end', () => {
  //     console.log(' csv file successfully Read ')
  // })
  // next();
};


/**
 * 
 * Final output
 * 
 * const csv = require("csv-parser");
const fs = require("fs");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const connection = require("../DataBase/db");
// const async = require('async')

module.exports.middle = (req, res, next) => {
  const { firstName, lastName, email, password } = req.body;
  let errors = [];
  if (!firstName || firstName == null || firstName.trim().length === 0)
    errors.push("firstName  required");
  if (!lastName || lastName == null || lastName.trim().length === 0)
    errors.push("lastName  required");
  if (!email || email == null || email.trim().length === 0)
    errors.push("email is required");
  if (!password || password == null || password.trim().length === 0)
    errors.push("password Number required");

  if (errors.length > 0) {
    return res.status(400).send({ msg: "Some Data Missing", errors });
  }

  req.body.firstName = firstName.trim();
  req.body.lastName = lastName.trim();
  req.body.email = email.trim();
  req.body.password = password.trim();

  // write the data on CSV file
  const sqlData = "SELECT * FROM userbasicinfo";

  connection.query(sqlData, (err, data) => {
    if (err) {
      return err;
    } else {
        const csvWriter = createCsvWriter({
        path: "data.csv",
        header: [
          { id: "firstName", title: "First Name" },
          { id: "lastName", title: "Last Name" },
          { id: "email", title: "Eamil ID" },
          { id: "password", title: "Password" },
          { id: "time", title: "Time Stamp" },
        ],
      });

      let usersArr=[];
      for (let i = 0; i < data.length; i++) {
            let obj = {
            firstName: data[i].firstName,
            lastName: data[i].lastName,
            email: data[i].email,
            password: data[i].password,
            time:  Date(),
          };
          usersArr.push(obj)
          
      }
      let reqData = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        time:  Date(),
      };
      usersArr.push(reqData)

      csvWriter.writeRecords(usersArr).then((csvData) => 
            console.log("csv data", csvData)
          ).catch((err)=>{ console.log("csv data", csvData) });

         
    }
  });

  // create the CSV file

  fs.createReadStream('data.csv').pipe(csv()).on('data', (row) => {
      console.log(row);
  }).on('end', () => {
      console.log(' csv file successfully Read ')
  })
  next();
};

 */