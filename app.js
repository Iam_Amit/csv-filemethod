const express = require('express');
const app = express();
// const db = require('./DataBase/db')
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))

const userRouter  = require('./user/route')

app.use(userRouter)

app.listen('5000', () => {
    console.log('The port is running on the server 5000 ')
})