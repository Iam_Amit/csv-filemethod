var mySql = require("mysql");

var connection = mySql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "userinfo",
});

connection.connect(function (err) {
  if (err) throw err;
  console.log("Database connected successfully !");
});

module.exports = connection;
